﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static int score;

    Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        scoreText = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();

    }
}
